package com.sda.xsi0;

import java.util.Scanner;

/**
 Method to generate game table
 Method for player to choose symbol x or 0 ( if invalid position => error message )
 Method to determine if there are 3 simultaneous symbols on a row,column or  2x diagonal
 */

public class Main {

    public static void main(String[] args) {
        String[][] board = generateBoard();

        printBoard(board);

        while(true) {
            setSymbol(board);
            printBoard(board);
            if(isWinner(board, "x") || isWinner(board, "0")){
                break;
            }
            else{
                System.out.println("Continue playing! ");
            }
        }
    }

    public static String[][] generateBoard(){
        String[][] board = new String[3][3];

        for(int i=0; i< board.length; i++){
            for(int j=0; j < board.length; j++){
                board[i][j] = "_";
            }
        }
        return board;
    }

    public static void printBoard(String[][] board){

        for(int i=0; i< board.length; i++){
            for(int j=0; j < board.length; j++){
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void setSymbol(String[][] board){
        Scanner sc = new Scanner(System.in).useDelimiter("\\n");
        String symbol = sc.next();

        Position symbolPosition = new Position(sc.nextInt(), sc.nextInt());
        symbolPosition.isPositionValid(board, sc);

        board[symbolPosition.row][symbolPosition.column] = symbol;
       // printBoard(board);

    }

    public static boolean isLineWinner(String[][] board, String symbol){
        boolean isTrue = true;
        boolean isWinner = false;
        for (int row = 0; row < board.length; row ++) {
            for (int col = 0; col < board.length; col++) {
                if (board[0][col].equals(symbol)) {
                    isTrue = isTrue && true;
                }else if(board[1][col].equals(symbol)){
                    isTrue = isTrue &&true;
                }else if(board[2][col].equals(symbol)){
                    isTrue = isTrue &&true;
                }else{
                    isTrue = false;
                }
            }
        }
        if(isTrue == true){
            isWinner = true;
        }
        return isWinner;
    }

    public static boolean isColumnWinner(String[][] board, String symbol){
        boolean isTrue = true;
        boolean isWinner = false;
        for (int row = 0; row < board.length; row ++) {
            for (int col = 0; col < board.length; col++) {
                if (board[row][0].equals(symbol)) {
                    isTrue = isTrue && true;
                }else if(board[row][1].equals(symbol)){
                    isTrue = isTrue &&true;
                }else if(board[row][2].equals(symbol)){
                    isTrue = isTrue &&true;
                }else{
                    isTrue = false;
                }
            }
        }
        if(isTrue == true){
            isWinner = true;
        }
        return isWinner;
    }
    public static boolean isPrincipalDiagonalWinner(String[][] board, String symbol){
        boolean isTrue = true;
        boolean isWinner = false;
        for (int row = 0; row < board.length; row ++) {
            for (int col = 0; col < board.length; col++) {
                if (row==col && board[row][col].equals(symbol)) {
                    isTrue = isTrue && true;
                }else{
                    isTrue = false;
                }
            }
        }
        if(isTrue == true){
            isWinner = true;
        }
        return isWinner;
    }

    public static boolean isSecondaryDiagonalWinner(String[][] board, String symbol){
        boolean isTrue = true;
        boolean isWinner = false;
        for (int row = 0; row < board.length; row ++) {
            for (int col = 0; col < board.length; col++) {
                if ((row+col) == (board.length-1) && board[row][col].equals(symbol)) {
                    isTrue = isTrue && true;
                }else{
                    isTrue = false;
                }
            }
        }
        if(isTrue == true){
            isWinner = true;
        }
        return isWinner;
    }

    public static boolean isWinner(String[][] board, String symbol){
        if(isLineWinner(board,symbol)){
            System.out.println("The winner is: "+symbol);
            return true;
        }
        if(isColumnWinner(board, symbol)){
            System.out.println("The winner is: "+symbol);
            return true;
        }
        if (isPrincipalDiagonalWinner(board, symbol)){
            System.out.println("The winner is: "+symbol);
            return true;
        }
        if(isSecondaryDiagonalWinner(board, symbol)){
            System.out.println("The winner is: "+symbol);
            return true;
        }
        return false;
    }

}
